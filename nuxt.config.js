/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
// Cabezera
import schema from './utils/schema'

const isProd = process.env.NODE_ENV === 'production'

const defaultSchema = {
  name: 'USMEF',
  title: 'USMEF México',
  url: 'https://www.usmef.org.mx',
  description:
    // eslint-disable-next-line max-len
    'U.S. Meat Export Federation, (USMEF), es una asociación sin fines de lucro que dedica todos sus esfuerzos a crear caminos más fáciles para la comercialización de la Carne Roja Americana de alta calidad en diferentes países del mundo.',
}

const meta = [
  {
    name: 'keywords',
    content: 'USMEF, carne, palabras, clave por llenar',
  },
  {
    name: 'google-site-verification',
    content: 'Jed-HEqd5HFV8vCgFCZOGSav13zUvsG73r4am7daIHM',
  },
]

const link = [
  {
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css?family=Encode+Sans:400,700',
  },
  {
    type: 'text/plain',
    rel: 'author',
    href: '/humans.txt',
  },
]
export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    meta,
    link,
    titleTemplate: (titleChunk = '') =>
      titleChunk ? `${titleChunk} - USMEF` : 'USMEF México',
    script: [
      {
        innerHTML: JSON.stringify(schema(defaultSchema)),
        type: 'application/ld+json',
      },
      {
        src:
          'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
        body: false,
      },
    ],
    htmlAttrs: {
      class: 'has-navbar-fixed-top',
    },
    __dangerouslyDisableSanitizers: ['script'],
  },
  /**
   * Variables de entorno para la página
   */
  env: {
    productionDomain: 'www.usmef.org.mx',
  },
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    // middleware: ['meta'],
  },
  /**
   * Transición por defecto
   */
  transition: 'page',
  /**
   * Crea un set para navegadores más vergras
   * https://nuxtjs.org/api/configuration-modern#the-modern-property
   */
  modern: isProd ? 'client' : false,
  /**
   * CSS global
   */
  css: [
    'vue2-animate/dist/vue2-animate.min.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
    '~assets/sass/transitions.scss',
    '~assets/sass/app.scss',
    '~assets/sass/global.scss',
    // // NOTE: Aquí puedes modificar el estilo de fontawesome
    // '@fortawesome/fontawesome-pro/css/fontawesome.css',
    // // '@fortawesome/fontawesome-pro/css/light.css',
    // '@fortawesome/fontawesome-pro/css/regular.css',
    // // '@fortawesome/fontawesome-pro/css/solid.css',
    // '@fortawesome/fontawesome-pro/css/brands.css',
  ],
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#19426a',
  },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~/plugins/font-awesome.js',
    '~plugins/buefy',
    {
      src: '~plugins/polyfills',
      ssr: false,
    },
    {
      src: '~plugins/scroll-track.js',
      nossr: true,
    },
  ],
  /*
   ** Build configuration
   */
  build: {
    /**
     * Enable thread-loader in webpack building
     */
    parallel: !isProd,
    /**
     * Enable cache of terser-webpack-plugin and cache-loader
     */
    cache: !isProd,
    /**
     * Es necesario sobreescribir lo que hace babel por defecto
     */
    babel: {
      presets: ({ isServer }) => [
        [
          require.resolve('@nuxt/babel-preset-app'),
          {
            buildTarget: isServer ? 'server' : 'client',
            // Incluir polyfills globales es mejor que no hacerlo
            useBuiltIns: 'entry',
            // Un poco menos de código a cambio de posibles errores
            loose: true,
            // Nuxt quiere usar ie 9, yo no.
            targets: isServer ? { node: 'current' } : {},
          },
        ],
      ],
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        // Si usas useBuiltIns: usage, descomenta el sig código
        // [
        //   '@babel/plugin-transform-runtime',
        //   {
        //     regenerator: true,
        //   },
        // ],
      ],
    },
    // Hace el css cacheable
    extractCSS: isProd,
    // Alias el ícono de buefy a uno que soporta los íconos de font-awesome
    plugins: [
      new (require('webpack')).NormalModuleReplacementPlugin(
        /buefy\/src\/components\/icon\/Icon\.vue/,
        require.resolve('./components/Buefy/Icon.vue')
      ),
      new (require('lodash-webpack-plugin'))(),
    ],
    /**
     * @param {Object} config The Build Config
     * @param {Object} ctx Nuxt Context
     * @returns {Object} config
     */
    extend(config, { isDev, isClient }) {
      if (isDev) {
        config.devtool = '#source-map'
      }
      // Evita conflictos con el bloque de documentación
      config.module.rules.push({
        resourceQuery: /blockType=docs/,
        loader: require.resolve('./utils/docs-loader.js'),
      })
      // Arregla pnpm
      function replaceLoaders(use) {
        if (use.loader) {
          if (use.loader.indexOf('babel-loader') !== -1) {
            use.loader = 'babel-loader'
          }
        } else if (use.indexOf('babel-loader') !== -1) {
          // eslint-disable-next-line no-param-reassign
          use = 'babel-loader'
        }
      }
      config.module.rules.forEach(rule => {
        if (rule.use) {
          rule.use.forEach(replaceLoaders)
        }
        if (rule.oneOf) {
          rule.oneOf.forEach(oneOf => {
            oneOf.use.forEach(replaceLoaders)
          })
        }
      })
      // Run ESLINT on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules(?!\/buefy)$/,
        })
      }
      return config
    },
  },
  /**
   * Estos modulos son altamente recomendado en producción ya que facilitan
   * la incluisión de varias herramientas
   */
  modules: [
    /**
     * Limpiador de CSS
     */
    'nuxt-purgecss',
    /**
     * Faebook pixel
     */
    [
      require.resolve('./modules/facebook-pixel'),
      {
        id: '661561957280518',
        disable: true,
      },
    ],
    /**
     * Google Analytics, se necesita un UA válido, se puede configurar para
     * envío a diferentes cuentas o vistas de acuerdo al entorno.
     */
    [
      '@nuxtjs/google-analytics',
      {
        // Puedes usar dos analíticas
        id: 'UA-129763518-1',
        disabled: true,
        autoTracking: {
          exception: true,
          page: true,
        },
        debug: {
          enabled: process.env.NODE_ENV !== 'production',
          sendHitTask: process.env.NODE_ENV === 'production',
        },
      },
    ],
    /**
     * PWA Este módulo integra todo lo necesario para implementar las
     * capacidades PWA a una página web, require ssl
     */
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          generateSW: true,
          // importScripts: ['/notifications-worker.js'],
          InjectManifest: true,
        },
      },
    ],
  ],
  /**
   * Configuración para PurgeCSS
   */
  purgeCSS: {
    whitelist: [
      'help',
      'fal',
      'has-icons-right',
      'dropdown',
      'modal',
      'dropdown-menu',
      'dropdown-content',
      'dropdown-item',
      'tag',
      'modal-background',
      'animation-content ',
      'modal-content',
    ],
    whitelistPatterns: [
      /loading-overlay/,
      /[\w|-]+-(enter|leave|move)-?(active|to)?/g,
    ],
    whitelistPatternsChildren: [/loading-overlay$/, /has-icons-.+$/, /^select/],
  },
  /**
   * Configuraciones que generan automáticamente manifest y
   * etiquetas básicas para seo
   */
  manifest: {
    name: defaultSchema.title,
    short_name: defaultSchema.name,
  },
  meta: {
    name: defaultSchema.name,
    author: 'Dinamo',
    description: defaultSchema.description,
    theme_color: '#274978',
    lang: 'es',
    nativeUI: false,
    mobileApp: true,
    appleStatusBarStyle: 'default',
    ogSiteName: true,
    ogTitle: true,
    ogDescription: true,
    ogImage: true,
    ogHost: 'https://www.usmef.org.mx/',
    ogUrl: true,
    twitterCard: true,
    twitterSite: 'https://www.usmef.org.mx/',
    twitterCreator: '@USMEF_Mex_CA_RD',
  },
}
