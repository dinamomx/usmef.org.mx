export default ({ store, route }) => {
  if (route.name !== store.state.route) {
    store.dispatch('metaChanged', {
      route: route.name,
      matched: route.meta,
      current: route.meta[route.meta.length - 1] || {},
    })
  }
}
