# Private

Este archivo está echo para mantener accesibles las claves necesarias para https.

- El CSR va como domain.csr
- La clave de renovación va como private.key
- El Certificado va como domain.crt
