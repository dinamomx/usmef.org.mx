import _ from 'lodash'

export default {
  data: () => ({
    selectedBrand: null,
    selectedCity: null,
  }),
  computed: {
    brands() {
      return this.locaciones.reduce((prev, current) => {
        const foundIndex = prev.findIndex(e => e.name === current.brand)
        if (foundIndex !== -1) {
          prev[foundIndex].cities.push(current.city)
        } else {
          prev.push({
            name: current.brand,
            cities: [current.city],
          })
        }
        return prev
      }, [])
    },
    cities() {
      return this.locaciones.reduce((prev, current) => {
        if (!prev.includes(current.city)) {
          prev.push(current.city)
        }
        return prev
      }, [])
    },
    availableCities() {
      if (this.selectedBrand) {
        const brands = this.brands.filter(
          brand => brand.name === this.selectedBrand
        )
        return brands.reduce(
          (prev, current) => _.union(prev, current.cities),
          []
        )
      }
      return this.cities
    },
    selected() {
      if (this.selectedBrand) {
        if (this.selectedCity) {
          // find by city and brand
          return this.locaciones.reduce((prev, current) => {
            if (
              current.city === this.selectedCity &&
              current.brand === this.selectedBrand
            ) {
              prev.push(current)
            }
            return prev
          }, [])
        }
        // find by brand
        return this.locaciones.reduce((prev, current) => {
          if (current.brand === this.selectedBrand) {
            prev.push(current)
          }
          return prev
        }, [])
      }
      if (this.selectedCity) {
        // find by city
        return this.locaciones.reduce((prev, current) => {
          if (current.city === this.selectedCity) {
            prev.push(current)
          }
          return prev
        }, [])
      }
      return this.locaciones
    },
  },
  methods: {
    beforeLeave(el) {
      const { marginLeft, marginTop, width, height } = window.getComputedStyle(
        el
      )
      el.style.left = `${el.offsetLeft - parseFloat(marginLeft, 10)}px`
      el.style.top = `${el.offsetTop - parseFloat(marginTop, 10)}px`
      el.style.width = width
      el.style.height = height
    },
  },
}
