/**
 * Este archivo es para aglomerar y configrurar los polyfills necesarios para
 *  cada proyecto
 */

// Es para dar soporte al css object-fit a IE/Edge
import objectFitImages from 'object-fit-images'

// Es para dar soporte a imagenes responsivas en IE/Edge 15 o menor
import picturefill from 'picturefill'
import 'picturefill/dist/plugins/mutation/pf.mutation.min'

window.onNuxtReady(() => {
  objectFitImages()
  picturefill()
})
