import Vue from 'vue'

import { library, config } from '@fortawesome/fontawesome-svg-core'
// import { faQuestionCircle } from '@fortawesome/pro-light-svg-icons'
// import { faQuestionCircle } from '@fortawesome/pro-solid-svg-icons'
import {
  faPlus,
  faQuestionCircle,
  faExclamationCircle,
  faEnvelope,
  faSearch,
  faTimes,
  faExternalLinkSquareAlt,
  faPhone,
  faFilePdf,
  faCalendar,
  faNewspaper,
  faArrowRight,
  faArrowLeft,
  faChevronDown,
} from '@fortawesome/pro-regular-svg-icons'
import {
  faFacebookMessenger,
  faFacebookSquare,
  faTwitterSquare,
  faInstagram,
  faYoutube,
  faWhatsapp,
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

config.familyPrefix = 'far'
config.autoAddCss = false

library.add([
  faPlus,
  faQuestionCircle,
  faEnvelope,
  faFacebookMessenger,
  faFacebookSquare,
  faTwitterSquare,
  faInstagram,
  faYoutube,
  faWhatsapp,
  faExclamationCircle,
  faSearch,
  faTimes,
  faFilePdf,
  faExternalLinkSquareAlt,
  faPhone,
  faCalendar,
  faNewspaper,
  faArrowRight,
  faArrowLeft,
  faChevronDown,
])
config.familyPrefix = 'far'

Vue.component('fa-icon', FontAwesomeIcon)
